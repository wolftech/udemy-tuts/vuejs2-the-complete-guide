const app = Vue.createApp({
    data() {
        return {
            myName: 'Kim Nybo Andersen',
            myAge: 42,
            imageURL: 'https://images.unsplash.com/photo-1632050815340-83f334858938?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80', 
        };
    },
    methods: {
        randomNumber() {
            return Math.random();
        },
        randomNumberAsString() {
            return this.randomNumber() + " " + this.randomNumber();
        },
    },
});

app.mount('#assignment');