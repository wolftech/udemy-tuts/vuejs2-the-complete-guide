const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      userName: '',
      userNameSignUp: '',
    };
  },
  methods: {
    onAddCallback(step) {
      this.counter += step;
    },
    onReduceCallback(step) {
      this.counter -= step;
    },
    onUserNameInput(evt) {
      this.userName = evt.target.value;
    },
    onUserNameSignUpInput(evt) {
      this.userNameSignUp = evt.target.value;
    },
    onFormSubmit(event) {
      alert("You wanted to submit: " + this.userNameSignUp);
    },
  },
});

app.mount('#events');
