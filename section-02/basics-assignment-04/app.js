const app = Vue.createApp({
    data() {
        return {
            userClass: '',
            pIsVisible: true,
            userBackgroundColor: '',
        };
    },
    computed: {
        styleMe() {
            console.log(this.userClass.includes('user1'));
            if (this.userClass.includes('user1') && this.pIsVisible) {
                return {
                    hidden: false,
                    visible: true,
                    user1: true,
                }
            }
            if (this.userClass.includes('user2') && this.pIsVisible) {
                return {
                    hidden: false,
                    visible: true,
                    user2: true,
                }
            }

            if (!this.pIsVisible) {
                return {
                    hidden: true,
                    visible: false,
                }
            }
        },
        pCustomStyle() {
            return {
                backgroundColor: this.userBackgroundColor,
            }
        },
    },
    methods: {
        toggleParagraph() {
            this.pIsVisible = !this.pIsVisible;
        },
    },
});

app.mount('#assignment');