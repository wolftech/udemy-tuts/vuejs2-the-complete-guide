const app = Vue.createApp({
    data() {
        return {
            alertMessage: 'Hey there, this is an alert.',
            inputTwoValue: '',
            inputThreeValue: '',
        };
    },
    methods: {
        onShowAlertClick(evt) {
            alert(this.alertMessage);
        },
        onInputTwoKeyDown(evt) {
            this.inputTwoValue = evt.target.value;
        },
        onInputThreeKeyDown(evt) {
            this.inputThreeValue = evt.target.value;
        }
    },
});

app.mount('#assignment');