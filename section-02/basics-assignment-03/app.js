const app = Vue.createApp({
    data() {
        return {
            result: 0,
            timerID: 0,
            timeLeft: 5,
            timeTaken: 0,
            gameMessage: 'Good Luck',
            gameStarted: false,
        };
    },
    watch: {
        result(value) {
            if (value == 0) {
                if (this.timerID > 0) {
                    clearInterval(this.timerID);
                    this.timerID = 0;
                }
                return;
            } else if (value > 0 && value < 37) {
                if (this.timerID > 0) {
                    this.timeLeft = 5;
                    return;
                }
            } else if (value == 37) {
                this.stopGame(true);
            }
        },
        timeLeft(value) {
            if (value <= 0) {
                this.stopGame(false);                
            }
        },
    },
    computed: {
        displayResult() {
            if (this.result == 37) {
                return this.result;
            } else if (this.result < 37) {
                return 'Not there yet.';
            } else if (this.result > 37) {
                return 'Too Much!';
            }
        },
        gameRunning() {
            return this.timerID > 0 || this.gameStarted == true ? true : false;
        },
        gameReset() {
            if (this.timeLeft == 5 && this.result == 0 && this.timeTaken == 0) {
                return true;
            } else {
                return false;
            }
        }
    },
    methods: {
        onAddToResult(evt, value) {
            this.result += value;
        },
        stopGame(won) {
            this.gameStarted = false;
            if (won) {
                this.gameMessage = "You WON the game, hitting 37 in " + this.timeTaken + " Seconds.";
            } else {
                this.gameMessage = "You LOST the game, ran out of time.";
            }
            clearInterval(this.timerID);
            this.timerID = 0;
        },
        onResetGame() {
            this.result = 0;
            this.timeLeft = 5;
            this.timeTaken = 0;
            this.gameMessage = 'Good Luck';
        },
        onStartGame() {
            this.gameStarted = true;
            const that = this;
            this.timerID = setInterval(function () {
                that.timeLeft--;
                that.timeTaken++;
            }, 1000);
        }
    },
});


app.mount('#assignment');