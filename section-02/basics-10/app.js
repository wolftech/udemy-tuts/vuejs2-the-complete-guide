const app = Vue.createApp({
    data() {
        return {
            boxASelected: false,
            boxBSelected: false,
            boxCSelected: false,
        };
    },
    computed: {
    },
    methods: {
        boxStyle(box) {
            if (box == 'A') {
                return {
                    'box-selected': this.boxASelected,
                };
            } else if (box == 'B') {
                return {
                    'box-selected': this.boxBSelected,
                }
            } else if (box == 'C') {
                return {
                    'box-selected': this.boxCSelected,
                }
            }
        },
        toggleBoxSelected(box) {
            if (box == 'A') {
                this.boxASelected = !this.boxASelected;
            } else if (box == 'B') {
                this.boxBSelected = !this.boxBSelected;
            } else if (box == 'C') {
                this.boxCSelected = !this.boxCSelected;
            }
        }
    },
});

app.mount('#styling');