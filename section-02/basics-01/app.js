const app = Vue.createApp({
    data() {
        return {
            courseGoalA: 'Finish the course, and learn Vue.js.',
            courseGoalB: 'Master Vue and build amazing apps.',
            linkToVue: 'https://vuejs.org/',
        };
    },
    methods: {
        outputGoal() {
            const randomNumber = Math.random();
            if (randomNumber < 0.5) {
                return this.courseGoalA;
            } else {
                return this.courseGoalB;
            }
        },
    },
});

app.mount('#user-goal');
