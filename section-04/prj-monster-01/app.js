const app = Vue.createApp({
    data() {
        return {
            monsterStats: {
                maxHealth: 100,
                curHealth: 100,
                damageMod: 0.85,
                healMod: 0.5,
                healChance: 0,
                level: 1,
            },
            playerStats: {
                maxHealth: 100,
                curHealth: 100,
                damageMod: 1.0,
                healMod: 1.0,
                level: 1,
                abilities: [
                    {
                        id: 1,
                        name: 'Attack',
                        damageDice: 8,
                        damageNum: 1,
                        cooldownTime: 2.0,
                        cooldownTimeID: 0,
                        onCoolDown: false,
                    },
                    {
                        id: 2,
                        name: "Special Attack",
                        damageDice: 8,
                        damageNum: 2,
                        cooldownTime: 3.5,
                        cooldownTimeID: 0,
                        onCoolDown: false,
                    },
                    {
                        id: 3,
                        name: 'Heal',
                        damageDice: 6,
                        damageNum: 2,
                        cooldownTime: 5.0,
                        cooldownTimeID: 0,
                        onCoolDown: false,
                    },
                ],
            },
            sounds: [
                {
                    id: 1,
                    file: 'sounds/Countdown_01.ogg',
                    audio: null,
                },
                {
                    id: 2,
                    file: 'sounds/Countdown_02.ogg',
                    audio: null,
                },
                {
                    id: 3,
                    file: 'sounds/Countdown_03.ogg',
                    audio: null,
                },
                {
                    id: 4,
                    file: 'sounds/Countdown_04.ogg',
                    audio: null,
                },
                {
                    id: 5,
                    file: 'sounds/Countdown_05.ogg',
                    audio: null,
                },
                {
                    id: 6,
                    file: 'sounds/Play.ogg',
                    audio: null,
                },
            ],
            countdownToStart: 5,
            gameStarted: false,
            countdownHasStarted: false,
            countdownTimerID: 0,
            gameOverLost: false,
            monsterActionTimerID: 0,
        };
    },
    computed: {
        computeMonsterHPWidth() {
            const percentCurrentHP = Math.floor((this.monsterStats.curHealth / this.monsterStats.maxHealth) * 100);
            return {
                width: percentCurrentHP + '%',
            }
        },
        computePlayerHPWidth() {
            const percentCurrentHP = Math.floor((this.playerStats.curHealth / this.playerStats.maxHealth) * 100);
            console.log(percentCurrentHP);

            return {
                width: percentCurrentHP + '%',
            }
        },
    },
    watch: {
        countdownToStart(value) {
            if (this.countdownHasStarted && value > 0) {
                this.playSoundByID(value);
            }
            if (value <= 0) {
                // Game starting.
                clearInterval(this.countdownTimerID);
                this.countdownTimerID = 0;
                this.countdownToStart = 5;
                this.countdownHasStarted = false;
                this.gameStarted = true;

                this.playSoundByID(6);

                // TODO: Start monster action timer.
            }
        },
    },
    methods: {
        onStartGame() {
            this.startCountDown();
        },
        startCountDown() {
            this.countdownHasStarted = true;
            const that = this;
            this.countdownTimerID = setInterval(() => {
                that.countdownToStart--;
            }, 1000);
            this.playSoundByID(5);
        },
        onCancelCountdown() {
            clearInterval(this.countdownTimerID);
            this.countdownTimerID = 0;
            this.countdownHasStarted = false;
            this.countdownToStart = 5;

        },
        cacheSounds() {
            this.sounds.forEach(element => {
                element.audio = new Audio ('/' + element.file);
            });
        },
        playSoundByID(id) {
            this.sounds.forEach((elem) => {
                if (elem.id == id) {
                    elem.audio.play();
                    return;
                }
            });
        },
        onSurrender() {
            this.gameStarted = false;
            this.gameOverLost = true;
        },
        onResetGame() {
            location.reload();
        },
        useAbility(abilityID) {
            const ability = this.playerStats.abilities.filter(e => e.id == abilityID)[0];
            alert("You used ability: " + ability.name);
        },
    },
    beforeMount() {
        this.cacheSounds();
    },
});



app.mount('#game');