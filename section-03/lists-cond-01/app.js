const app = Vue.createApp({
  data() {
    return { 
      inputGoalEntered: '',
      goals: [],
      nextID: 1,
    };
  },
  computed: {
    hasGoals() {
      return this.goals.length > 0;
    },
    addShouldBeDisabled() {
      return (this.inputGoalEntered == '');
    },
  },
  methods: {
    onAddGoal() {
      if (this.inputGoalEntered == '')
        return;

      const newGoal = {
        id: this.nextID++,
        content: this.inputGoalEntered,
        isComplete: false
      };
      this.goals.push(newGoal);
      this.inputGoalEntered = '';
    },
    onRemoveClick(goalID) {
      this.goals = this.goals.filter(goal => goal.id != goalID);
    },
    toggleIsComplete(goalID) {
      this.goals.forEach(element => {
        if (element.id == goalID) {
          element.isComplete = !element.isComplete;
        }
      });
    },
  },
});

app.mount('#user-goals');
