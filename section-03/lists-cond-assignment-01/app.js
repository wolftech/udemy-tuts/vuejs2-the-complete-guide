const app = Vue.createApp({
    data() {
        return {
            newTaskInput: '',
            tasks: [],
            nextID: 1,
            isTaskListShown: true,
            hideBtnText: 'Hide List',
        };
    },
    methods: {
        onAddTaskClick() {
            if (this.newTaskInput == '')
                return;
            const newTask = {
                id: this.nextID++,
                content: this.newTaskInput
            };
            this.tasks.push(newTask);
            this.newTaskInput = '';
        },
        hideBtnClick() {
            this.isTaskListShown = !this.isTaskListShown;
            this.hideBtnText = this.isTaskListShown ? 'Hide list' : 'Show List';
        },
    },
    computed: {

    },
});

app.mount('#assignment');